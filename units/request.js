// 请求封装

// 基础地址
const BASE_URL = 'https://api-hmugo-web.itheima.net/api/public/v1'

// 基本封装
export const Request = (options) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: BASE_URL + options.url,
			method: options.method || 'GET',
			success: res => {
				if(res.statusCode == 200) {
					uni.showToast({
						title: '获取成功',
						icon: 'success'
					})
					return resolve(res)
				}
			},
			fail: err => {
				reject(err)
			}
		})
	})
}
