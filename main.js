import Vue from 'vue'
import App from './App'
import { Request } from 'units/request.js'

Vue.config.productionTip = false

Vue.prototype.$http = Request

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
